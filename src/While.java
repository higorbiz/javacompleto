import java.util.Locale;
import java.util.Scanner;

public class While {

	public static void main(String[] args) {

		/*
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		System.out.println("Digite um número");
		double numero = sc.nextDouble();

		while (numero >= 0.0) {
			double sr = Math.sqrt(numero);
			System.out.printf("%.3f%n", sr);
			System.out.println("Digite um número novamente");
			numero = sc.nextDouble();
		}
		System.out.println("Número negativo");

		sc.close();
*/
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Escolha para abastecer");
		System.out.println("1 - Álcool");
		System.out.println("2 - Gasolina");
		System.out.println("3 - Diesel");
		System.out.println("4 - Sair");
		int numero = sc.nextInt();
		
		int alcCount = 0;
		int gasoCount = 0;
		int disCount = 0;
		
		
		while (numero != 4) {
			if (numero == 1) {
				alcCount = alcCount +1;
			}
			else if (numero == 2) {
				gasoCount = gasoCount +1;
			}
			else if (numero == 3) {
				disCount = disCount +1;
			}	
			
			numero = sc.nextInt();
				
			}
			
			System.out.println("Álcool - " + alcCount);
			System.out.println("Gasolina - " + gasoCount);
			System.out.println("Diesel - " +  disCount);
	
		System.out.println("Muito Obrigado!");

		sc.close();
		
		
	}

}