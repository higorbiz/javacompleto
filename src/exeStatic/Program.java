package exeStatic;

import java.util.Locale;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
	
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Qual o preço do dollar?");
		double dollar = sc.nextDouble();
		
		System.out.println("Quantos dollares vc quer comprar?");
		int quantity = sc.nextInt();
		
		double result = CurrencyConverter.calcDollar(quantity, dollar);
		
		System.out.printf("Qual o preço do dollar? = %.2f%n ", dollar);
		System.out.println("Quantos dollares vc quer comprar? " + quantity);
		System.out.printf("Valor  total = %.2f%n ", result);
		
		sc.close();

	}

}
