package exe2Composition;

public enum OrderStatus {

	PENDING_PAYMENT, PROCESSING, SHIPPED, DELIVERED
}
