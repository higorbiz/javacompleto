package excecoes;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Program2Pilha {

	public static void main(String[] args) {

		method1();
		
	}
	
	public static void method1() {
		System.out.println("***Method1 start***");
		method2();
		System.out.println("***Method1 end***");
	}

	public static void method2() {
		System.out.println("***Method2 start***");
		Scanner sc = new Scanner(System.in);

		try {
			String[] vect = sc.nextLine().split(" ");
			int position = sc.nextInt();
			System.out.println(vect[position]);
		}

		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Invalid position!!");
			e.printStackTrace();
			sc.hasNext();
		}

		catch (InputMismatchException e) {
			System.out.println("Input error!!");
		}

		System.out.println("End of program!!");
		sc.close();
		System.out.println("***Method2 end***");
	}

}
