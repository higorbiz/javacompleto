package exeContaBancaria;

import java.util.Locale;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		double initialDeposit = 0;
		
		

		System.out.println("Enter account number: ");
		int account = sc.nextInt();
		
		System.out.println("Enter account holder: ");
		String holder = sc.next();
		
		System.out.println("Is there a initial deposity (y/n)?");
		char yesNo = sc.next().charAt(0);
		
		if (yesNo == 'y') { 
		System.out.println("Enter initial deposit value: ");
	    initialDeposit = sc.nextDouble();
	    
		} else {
			initialDeposit = 0;
		}
		
		
		Conta conta = new Conta(account, holder, initialDeposit);
		conta.setAccountValue(initialDeposit);
		
		System.out.println("Account data:");
		System.out.println("Account: " + conta.getAcountNumber());
		System.out.println("Holder: " + conta.getHolder());
		System.out.printf("%.2f%n ", conta.getAccountValue());
		
		// depósito
		System.out.println("Enter deposit value: ");
		double deposit = sc.nextDouble();
		double deposito = conta.deposit(deposit);
		conta.setAccountValue(deposito);
		
		System.out.println("Account data:");
		System.out.println("Account: " + conta.getAcountNumber());
		System.out.println("Holder: " + conta.getHolder());
		System.out.printf("%.2f%n ", conta.getAccountValue());
		
		//retirada
		System.out.println("Enter withdraw value: ");
		double withdraw = sc.nextDouble();
		double retirada = conta.withdraw(withdraw);
		conta.setAccountValue(retirada);
		
		System.out.println("Account data:");
		System.out.println("Account: " + conta.getAcountNumber());
		System.out.println("Holder: " + conta.getHolder());
		System.out.printf("%.2f%n ", conta.getAccountValue());
		
		sc.close();
	}

}
