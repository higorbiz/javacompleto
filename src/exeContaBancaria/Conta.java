package exeContaBancaria;

public class Conta {
	
	private int acountNumber;
	private String holder;
	private double accountValue;
	
	
	public int getAcountNumber() {
		return acountNumber;
	}
	public void setAcountNumber(int acountNumber) {
		this.acountNumber = acountNumber;
	}
	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	public double getAccountValue() {
		return accountValue;
	}
	public void setAccountValue(double accountValue) {
		this.accountValue = accountValue;
	}
	
	public Conta(int acountNumber, String holder, double accountValue) {

		this.acountNumber = acountNumber;
		this.holder = holder;
		this.accountValue = accountValue;
	}
	
	public Conta(int acountNumber, String holder) {

		this.acountNumber = acountNumber;
		this.holder = holder;
	}
	
	
	public double deposit(double valor) {
		
		double deposit = accountValue + valor;
		
		return deposit;
		
	}
	
public double withdraw(double value) {
		
		double withdraw = 0;
		withdraw = accountValue - value - 5.00;
		
		return withdraw;
		
	}
	
	
	

}
