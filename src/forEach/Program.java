package forEach;

public class Program {

	public static void main(String[] args) {

		String[] nomes = new String[] { "Maria", "Bob", "Alex" };
		
		for (int i = 0; i < nomes.length; i++) {
			System.out.println(nomes[i]);
		}
		
		System.out.println("-----------------------------------");
		
		for (String nome : nomes) {
			System.out.println(nome);
		}
		
		

	}

}
