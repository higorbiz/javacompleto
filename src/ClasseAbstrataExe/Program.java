package ClasseAbstrataExe;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import ClasseAbstrataExeResolvido.Shape;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		List<Contribuinte> list = new ArrayList<>();
		
		System.out.println("Enter the number of tax payers");
		int n = sc.nextInt();
		
		for (int i = 1; i <= n; i++) {
			
			System.out.print("Tax payer # "+ i + " data:");
			System.out.println("Individual or company (i/c)?");
			char ch = sc.next().charAt(0);
			
			if (ch == 'i') {
			
			System.out.println("Name: ");
			String name = sc.nextLine();
			sc.nextLine();
			
			System.out.println("Anual income: ");
			Double income = sc.nextDouble();
			
			System.out.println("Health expenditures: ");
			Double health = sc.nextDouble();
			
			list.add(new PessoaFisica(name, income, health));
			} else {
				
				System.out.println("Name: ");
				String name = sc.nextLine();
				sc.nextLine();
				
				System.out.println("Anual income: ");
				Double income = sc.nextDouble();
				
				System.out.println("Number of employees: ");
				int employees = sc.nextInt();
				
				list.add(new PessoaJuridica(name, income, employees));
				
			}
			
			
			}
			
		System.out.println("Taxes Paid");
		for (Contribuinte c  : list) {
			System.out.println(String.format("%.2f", c.calcTaxes()));
		}
		
		
		Double sum = 0.0;
		for (Contribuinte c : list ) {
			sum += c.calcTaxes();
		}
		System.out.println(String.format("Total Taxes: " + "%.2f", sum));
		
		
		

		sc.close();
	}

}
