package ClasseAbstrataExe;

public class PessoaFisica extends Contribuinte {

	private Double healthSpend;

	public PessoaFisica() {
		super();
	}

	public PessoaFisica(String name, Double anualIncome, Double healthSpend) {
		super(name, anualIncome);
		this.healthSpend = healthSpend;
	}

	public Double getHealthSpend() {
		return healthSpend;
	}

	public void setHealthSpend(Double healthSpend) {
		this.healthSpend = healthSpend;
	}

	@Override
	public Double calcTaxes() {
		Double valueToPay = 0.0;
		if(anualIncome < 20000.00) {
			valueToPay = (anualIncome * 15 / 100) - (healthSpend * 50 /100); 
		}else if(anualIncome >= 20000.00){
			valueToPay = (anualIncome * 25 / 100) - (healthSpend * 50 /100);
		}
		
		return valueToPay;
	}

}
