package ClasseAbstrataExe;

public class PessoaJuridica extends Contribuinte{

	private int numberOfEmployee;
	
	public PessoaJuridica() {
		super();
	}

	public PessoaJuridica(String name, Double anualIncome, int numberOfEmployee) {
		super(name, anualIncome);
		this.numberOfEmployee = numberOfEmployee;
	}

	public int getNumberOfEmployee() {
		return numberOfEmployee;
	}

	public void setNumberOfEmployee(int numberOfEmployee) {
		this.numberOfEmployee = numberOfEmployee;
	}
	
	@Override
	public Double calcTaxes() {
		Double valueToPay = 0.0;
		if(numberOfEmployee > 10) {
			valueToPay = anualIncome * 14 / 100; 
		}else {
			valueToPay = anualIncome * 16 / 100;
		}
		
		return valueToPay;
	}

}
