package Interfaces2;

public interface TaxService {
	
	double tax(double amount);

}
