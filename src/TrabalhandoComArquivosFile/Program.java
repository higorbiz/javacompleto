package TrabalhandoComArquivosFile;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {

		File file = new File("/Users/higorbiz/Documents/teste.txt");
		Scanner sc = null;

		try {
			sc = new Scanner(file); // recebe file ao invés de System.in
			while (sc.hasNextLine())// enquanto tiver linha vai lendo
			{
				System.out.println(sc.nextLine()); // imprime o que for lido
			}
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage()); // se tiver erro captura o erro
		}
		finally {
			if (sc != null) {
				sc.close();
			}
		}
	}

}
