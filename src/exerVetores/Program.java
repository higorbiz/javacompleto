package exerVetores;

import java.util.Scanner;

import Vetor2.Product;

public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		Rent[] vect = new Rent[9];

		System.out.println("Digite a quantidade de quartos que vai usar!");
		int counterRooms = sc.nextInt();

		for (int i = 0; i < counterRooms; i++) {
		
		System.out.println("Digite o número do quarto");
		int room = sc.nextInt();

		if (vect[room] != null) {

			System.out.println("quarto ocupado");

		} else {
			sc.nextLine();
			System.out.println("Digite o nome");
			String name = sc.nextLine();

			sc.nextLine();
			System.out.println("Digite o email");
			String email = sc.nextLine();

			vect[room] = new Rent(name, email);
		}
		}

		for (int i = 0; i < vect.length; i++) {

			if (vect[i] != null) {
				System.out.println("Quarto: " + i + " Nome: " + vect[i].getName() + " Email: " + vect[i].getEmail());
			} 
		}
		
		for (int i = 0; i < vect.length; i++) {
			if (vect[i] == null) {
			System.out.println("Quartos desocupados: " + i );
			}
		}

		sc.close();
	}
}
