package TrabalhandoComArquivosPastas;

import java.io.File;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter a folder path: ");
		String strPath = sc.nextLine(); // /Users/higorbiz/Documents
		
		File path = new File(strPath);
		File[] folders = path.listFiles(File::isDirectory); // lista as pastas
		
		System.out.println("FOLDERS:");
		for (File folder : folders) {
			System.out.println(folder);
		}
		
		File[] files = path.listFiles(File::isFile); // lista os arquivos
		System.out.println("FILES:");
		
		for (File file : files) {
			System.out.println(file);
		}
		
		boolean success = new File(strPath + "/subdir").mkdir();
		System.out.println("Directory created successfully: " + success);
		sc.close();

	}

}
