package TrabalhandoComArquivosFileWriterBufferedWriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Program {

	public static void main(String[] args) {

		String[] lines = new String[] { "Good morning", "Good afternoon", "Good night" };
		String path = "/Users/higorbiz/Documents/teste2.txt";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(path,true))) { // true serve para não recriar o arquivo novamente
			for (String line : lines) {
				bw.write(line);
				bw.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
