package excecoesExercicioFixacao;

import java.text.ParseException;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) throws ParseException{
		
		Scanner sc = new Scanner(System.in);

		
		System.out.println("Enter account data");
		System.out.println("Number: ");
		int number = sc.nextInt();
		
		System.out.println("Holder: ");
		String holder = sc.nextLine();
		sc.nextLine();
		
		System.out.println("Initial balance: ");
		Double initialBalance = sc.nextDouble();
		sc.nextLine();
		
		System.out.println("Withdraw limit: ");
		Double withdrawLimit = sc.nextDouble();
		sc.nextLine();
		
		Account account = new Account(number, holder, initialBalance, withdrawLimit);
		
		System.out.println("Enter amount for withdraw: ");
		Double amount = sc.nextDouble();
		
		try {
			account.withdraw(amount);
			System.out.println("New balance: " + String.format("%.2f", account.getBalance()));
		}
		
		catch (DomainException e) {
			System.out.println("Withdraw error: " + e.getMessage());
		}
		
	}

}
