package GenericsExe1;

import java.util.Scanner;



public class Program {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		//PrintService<Integer> ps = new PrintService<>(); //Pode ser assim tbm new PrintService<Integer>()
		
		PrintService<String> ps = new PrintService<>();
		
		System.out.println("How many values? ");
		
		int n = sc.nextInt();
		
		for (int i = 0; i < n; i++) {
			//Integer value = sc.nextInt();
			String value = sc.next();
			ps.addValue(value);
		}
		
		ps.print();
		//Integer x = ps.first();
		String x = ps.first();
		System.out.println("First: " + x);
		
		sc.close();

	}

}
