package equalsAndHashCode;

public class Program {

	public static void main(String[] args) {

		//equals
//		String a = "Maria";
//		String b = "Maria";
//		System.out.println(a.equals(b));
		
		
		// hashCode
//		String c = "Maria";
//		String d = "Maria";
//		System.out.println(c.hashCode());
//		System.out.println(d.hashCode());
		//==============================
		
		
		Client c1 = new Client("Maria", "maria@gmail.com");
		Client c2 = new Client("Maria", "maria@gmail.com");
		
		
			String s1 = "Test";
			String s2 = "Test";

		System.out.println(c1.hashCode());
		System.out.println(c2.hashCode());
		System.out.println(c1.equals(c2));
		System.out.println(c1 == c2); // Da falso porque ele compara a posição da memória e não o conteúdo. As referências de memória são diferentes
		System.out.println(s1 == s2); // A comparação é literal é diferente, tem um tratamento especial. só seria diferente se tivesse o new String("Test);
	}

}
